@extends("admin.layout")
@section("content")

    @component("components.headerWithToolbar",["title"=>"ویدئو جدید"])
        <a href="{{ route("admin.videos.index") }}" class="btn btn-outline-dark ">ویدئو ها</a>
    @endcomponent
    <form action="{{ route("admin.videos.store") }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="form-group col">
                        <label for="title">عنوان پست / ویدئو</label>
                        <input class="form-control" type="text" required name="title" id="title" value="{{ old("title") }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="code">کد / نام فایل بدون پسوند</label>
                        <input class="form-control" type="text" required name="code" id="code" value="{{ old("code") }}">
                    </div>
                    <div class="form-group col">
                        <label for="publish_at">تاریخ انتشار</label>
                        <input class="form-control" type="text" required name="publish_at" id="publish_at" value="{{ old("publish_at") }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="body">کپشن</label>
                        <input class="form-control" type="text" name="body" id="body" value="{{ old("body") }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="instagram_id">آی دی پست در اینستاگرام</label>
                        <input class="form-control" type="text" name="instagram_id" id="instagram_id" value="{{ old("instagram_id") }}">
                    </div>
                    <div class="form-group col">
                        <label for="instagram_link">لینک پست اینستاگرام</label>
                        <input class="form-control" type="text" name="instagram_link" id="instagram_link" value="{{ old("instagram_link") }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="instagram_likes">تعداد لایک اینستاگرام</label>
                        <input class="form-control" type="text" name="instagram_likes" id="instagram_likes" value="{{ old("instagram_likes") }}">
                    </div>
                    <div class="form-group col">
                        <label for="instagram_views">تعداد بازدید اینستاگرام</label>
                        <input class="form-control" type="text" name="instagram_views" id="instagram_views" value="{{ old("instagram_views") }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="instagram_comments">تعداد کامنت اینستاگرام</label>
                        <input class="form-control" type="text" name="instagram_comments" id="instagram_comments" value="{{ old("instagram_comments") }}">
                    </div>
                    <div class="form-group col">
                        <label for="instagram_datetime">تاریخ و زمان پست اینستاگرام</label>
                        <input class="form-control" type="text" name="instagram_datetime" id="instagram_datetime" value="{{ old("instagram_datetime") }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="youtube_url">لینک یوتیوب</label>
                        <input class="form-control" type="text" name="youtube_url" id="youtube_url" value="{{ old("youtube_url") }}">
                    </div>
                    <div class="form-group col">
                        <label for="facebook_url">لینک فیسبوک</label>
                        <input class="form-control" type="text" name="facebook_url" id="facebook_url" value="{{ old("facebook_url") }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="aparat_url">لینک آپارات</label>
                        <input class="form-control" type="text" name="aparat_url" id="aparat_url" value="{{ old("aparat_url") }}">
                    </div>
                </div>
                <div class="form-group">
                    <input name="" value="ثبت" type="submit" class="btn btn-primary">
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="form-group col">
                        <label for="title">عکس</label>
                        <input class=""  required name="image" type="file">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'body' );
    </script>
@endsection

@push("script")
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    </script>
@endpush