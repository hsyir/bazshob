@extends("admin.layout")
@section("content")

    @component("components.headerWithToolbar",["title"=>"پلن جدید"])
        <a href="{{ route("admin.plans.index") }}" class="btn btn-outline-dark ">پلن ها</a>
    @endcomponent


    <form action="{{ route("admin.plans.update",$plan) }}" method="post">
        {{ csrf_field() }}
        {{ method_field("PUT") }}
        <div class="row">
            <div class="col-8">

                <div class="row">
                    <div class="form-group col">
                        <label for="title">عنوان</label>
                        <input class="form-control" type="text" name="title" id="title" value="{{ old("title",$plan->title) }}">
                    </div>
                    <div class="form-group col">
                        <label for="price">مبلغ</label>
                        <input class="form-control" type="text" name="price" id="price" value="{{ old("price",$plan->price) }}">
                    </div>
                    {{--<div class="form-group col">--}}
                        {{--<label for="published">منتشر شده</label>--}}
                        {{--<input class="form-control" type="text" name="published" id="published" value="{{ old("published") }}">--}}
                    {{--</div>--}}
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="summary">توضیحات مختصر</label>
                        <input class="form-control" type="text" name="summary" id="summary" value="{{ old("summary",$plan->summary) }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="descriptions">توضیحات</label>
                        <input class="form-control" type="text" name="descriptions" id="descriptions" value="{{ old("descriptions",$plan->descriptions) }}">
                    </div>
                </div>
                <div class="form-group">
                    <input name="" value="ثبت" type="submit" class="btn btn-primary">
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="form-group col">
                        <label for="title">عنوان</label>

                        <input class="" name="image" type="file">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'descriptions' );
    </script>
@endsection

@push("script")
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    </script>
@endpush