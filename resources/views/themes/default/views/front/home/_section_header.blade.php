<div class="pb-5 ">
    <h2 class="h3 text-center">
        <div class="title">
        {{ $title }}
        </div>
    </h2>
    <div class="text-center ">
        {{ $slot }}
    </div>
</div>