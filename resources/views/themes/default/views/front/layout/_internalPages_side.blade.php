<div class="">
    <div class="widgets ">
        @component('front.layout.widget_frame',['title'=>Options::getSiteOption('site-title')])
            <div class="p-3">
                {!! nl2br(Options::getSiteOption('site-description'))  !!}
            </div>
        @endcomponent
        {{--@component('front.layout.widget_frame',['title'=>'آخرین نوشته ها'])
            @widget('contents.last-blog-posts')
        @endcomponent
        @component('front.layout.widget_frame',['title'=>'برچسب ها'])
            @widget('contents.tags-list')
        @endcomponent--}}
    </div>

</div>