<section id="footer" class="bg-dark px-5 pt-3 pb-4">
    <div class="container  text-white pb-5 pb-lg-0">
        <div class="row ">
            <div class="col-md-4 pb-4 pb-md-0">
                <div class="row">
                    <h4 class="col-12 h4">{{ Options::getSiteOption('site-title') }}</h4>
                </div>
                <div class="row">
                    <div class="col-12 small">{{ Options::getSiteOption('site-short-description') }}</div>
                </div>
            </div>
            <div class="col-md-4 pb-4 pb-md-0">
            </div>
            <div class="col-md-4 pb-4 pb-md-0 text-md-left">
                <div class="row">
                    <div class="col-12">
                        <ul dir="ltr" class="text-left contact_information p-0 m-0">
                            <li>
                                <i class="fa fa-envelope"></i>
                                <a class="text-white small"
                                   href="mailto:{{ Options::getSiteOption('site-email') }}">{{ Options::getSiteOption('site-email') }}</a>
                            </li>
                            <li><i class="fa fa-phone"></i> <a href="tel:1709"
                                                               class="text-white">{{ Options::getSiteOption('site-tell') }}
                                </a></li>
                        </ul>

                    </div>

                    <div class="col-md-12 text-left ">
                        <div class="social-icons">
                           {{-- <a href="https://www.facebook.com/">
                                <i id="social-fb" class="fa fa-facebook  "></i></a>
                            <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter  "></i></a>
                            <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-telegram  "></i></a>
                            <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-instagram  "></i></a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row copyright border-top">
            <div class="col-12 small pt-3" style="color: #999;">
                تمام حقوق مادی و معنوی این سایت متعلق به <strong><a href="{{ url("/") }}">{{ Options::getSiteOption('site-title') }}</a></strong> است

                <span class="float-left"> قدرت گرفته از <a href="http://sazimo.ir" target="_blank">سازیمو</a></span>
            </div>
        </div>
    </div>


    {{--<div class="fixed-bottom d-lg-none bg-white">
        <div class="container">
            <div class="row ">
                <div class="col-12 bg-light rounded border m-2 p-2 text-center">
                    برای سفارش همین حالا کلیک کنید
                    <a href="tel:1709" class="btn btn-success text-white">1709 <i class="fa fa-phone"></i></a>
                </div>
            </div>
        </div>
    </div>--}}
</section>