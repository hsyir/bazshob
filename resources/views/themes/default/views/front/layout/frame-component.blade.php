@php
    $text_class = isset($options->text_class)?$options->text_class : "";
@endphp
@if( !isset($options->bg_type) or $options->bg_type=="BG_DEFAULT")
    <section class="{{ $text_class }}">
        <div class="container">
            {{ $slot }}
        </div>
    </section>
@elseif($options->bg_type=="BG_IMAGE")
    @if($options->bg_image_type == "BG_IMAGE_TYPE_FIX")
        <section style="background:  url({{ $options->bg_image_url }}) ;background-size: cover" class="{{ $text_class }}">
            <div class="container">
                {{ $slot }}
            </div>
        </section>
    @elseif($options->bg_image_type == "BG_IMAGE_TYPE_PARALLAX")

        <section class="{{ $text_class }}">
            <div class="jarallax " data-jarallax data-speed="{{ $options->bg_parallax_speed }}">
                <img class="jarallax-img" src="{{ $options->bg_image_url }}" alt="">
                <div class="container">
                    {{ $slot  }}
                </div>
            </div>
        </section>


    @endif

@elseif($options->bg_type=="BG_TRANSPARENT")
@elseif($options->bg_type=="BG_SOLID")

    <section id="home_prices" class="{{ $options->bg_class }}  {{ $text_class }}">
        <div class="container">

            {{ $slot }}

        </div>

    </section>

@endif