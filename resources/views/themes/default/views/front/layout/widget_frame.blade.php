<div class="border rounded mb-3 ">
    <h6 class="h6 m-0 py-2 bg-primary text-white text-center rounded-top">{{ $title }}</h6>
    <div>
        {{ $slot }}
    </div>
</div>