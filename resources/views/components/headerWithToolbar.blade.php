{{--backend/components/section--}}
<div
        class="d-flex justify-content-between flex-wrap
         flex-md-nowrap align-items-center pb-2 mb-3 mt-3 border-bottom" >
    <h1 class="h5">{!! $title !!}</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        {{$slot}}
    </div>
</div>


@push('scripts-')
    <script>
        $(document).ready(function () {
            var fnav = $('#headt');
            var topnav = $('.sticky-top').height();
            var top = fnav.offset().top - topnav;
            $(window).scroll(function (event) {
                var y = $(this).scrollTop();
                if (y >= top)
                    fnav.css('top', y-10)
                else
                    fnav.css('top', top-10)
            });
        })
    </script>
@endpush