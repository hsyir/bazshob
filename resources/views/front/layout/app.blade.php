<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>زود نیوز</title>
    @include('front.layout._meta')
    @include('front.layout._fav_icon')

    <link rel="stylesheet" href="{{  asset("front/css/app.css") }}">
    @stack("scripts-top")
</head>
<body class="rtl ">
@include('front.layout._nav_front')
<main id="app" style="display: none;" >
    <div class="content-container p-0 m-0">
        @yield("content")
    </div>

    @include("front.layout._footer")
</main>
<script src="{{  asset('/front/js/app.js') }}"></script>
@stack("scripts-bottom")
</body>
</html>
