@extends("front.layout.app")
@section('content')
    <div class="bg1">
        <div class=" container pt-3 pb-3">
            <div class="row">
                <div class="col-md-8  ">
                    <div class="card">
                        <div class="card-title">
                            <div class="h4">
                                {{ $plan->title }}
                            </div>
                        </div>
                        <div class="card-body">
                            {!! nl2br( $plan->description) !!}
                        </div>
                    </div>
                </div>


                <div class="col-md-4  ">
                    <div class="card ">
                        <div class="card-body">
                            <img src="{{ $plan->getFirstMediaUrl('image') }}" class="w-100">
                        </div>
                    </div>
                    <div class="card  ">
                        <div class="card-title">
                            <div class="h5">
                                {{ $plan->title }}
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-12 ">
                                    قیمت: <div class="h4">{{ number_format($plan->price) }} تومان</div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <form action="{{ route('user.invoices.create') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                                        <input type="submit" class="btn btn-outline-success" value="صدور فاکتور خرید"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>
@endsection