@extends("front.layout.app")
@section('content')

    @push('scripts-top')

        <style>
            #ad-introduce #img-holder {
                background-color: #fff41c;
                background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='100%25' height='100%25' viewBox='0 0 800 800'%3E%3Cdefs%3E%3CradialGradient id='a' cx='400' cy='400' r='50%25' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='0' stop-color='%23fff41c'/%3E%3Cstop offset='1' stop-color='%23ff0f0f'/%3E%3C/radialGradient%3E%3CradialGradient id='b' cx='400' cy='400' r='70%25' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='0' stop-color='%23fff41c'/%3E%3Cstop offset='1' stop-color='%23ff4242'/%3E%3C/radialGradient%3E%3C/defs%3E%3Crect fill='url(%23a)' width='800' height='800'/%3E%3Cg fill-opacity='.8'%3E%3Cpath fill='url(%23b)' d='M998.7 439.2c1.7-26.5 1.7-52.7 0.1-78.5L401 399.9c0 0 0-0.1 0-0.1l587.6-116.9c-5.1-25.9-11.9-51.2-20.3-75.8L400.9 399.7c0 0 0-0.1 0-0.1l537.3-265c-11.6-23.5-24.8-46.2-39.3-67.9L400.8 399.5c0 0 0-0.1-0.1-0.1l450.4-395c-17.3-19.7-35.8-38.2-55.5-55.5l-395 450.4c0 0-0.1 0-0.1-0.1L733.4-99c-21.7-14.5-44.4-27.6-68-39.3l-265 537.4c0 0-0.1 0-0.1 0l192.6-567.4c-24.6-8.3-49.9-15.1-75.8-20.2L400.2 399c0 0-0.1 0-0.1 0l39.2-597.7c-26.5-1.7-52.7-1.7-78.5-0.1L399.9 399c0 0-0.1 0-0.1 0L282.9-188.6c-25.9 5.1-51.2 11.9-75.8 20.3l192.6 567.4c0 0-0.1 0-0.1 0l-265-537.3c-23.5 11.6-46.2 24.8-67.9 39.3l332.8 498.1c0 0-0.1 0-0.1 0.1L4.4-51.1C-15.3-33.9-33.8-15.3-51.1 4.4l450.4 395c0 0 0 0.1-0.1 0.1L-99 66.6c-14.5 21.7-27.6 44.4-39.3 68l537.4 265c0 0 0 0.1 0 0.1l-567.4-192.6c-8.3 24.6-15.1 49.9-20.2 75.8L399 399.8c0 0 0 0.1 0 0.1l-597.7-39.2c-1.7 26.5-1.7 52.7-0.1 78.5L399 400.1c0 0 0 0.1 0 0.1l-587.6 116.9c5.1 25.9 11.9 51.2 20.3 75.8l567.4-192.6c0 0 0 0.1 0 0.1l-537.3 265c11.6 23.5 24.8 46.2 39.3 67.9l498.1-332.8c0 0 0 0.1 0.1 0.1l-450.4 395c17.3 19.7 35.8 38.2 55.5 55.5l395-450.4c0 0 0.1 0 0.1 0.1L66.6 899c21.7 14.5 44.4 27.6 68 39.3l265-537.4c0 0 0.1 0 0.1 0L207.1 968.3c24.6 8.3 49.9 15.1 75.8 20.2L399.8 401c0 0 0.1 0 0.1 0l-39.2 597.7c26.5 1.7 52.7 1.7 78.5 0.1L400.1 401c0 0 0.1 0 0.1 0l116.9 587.6c25.9-5.1 51.2-11.9 75.8-20.3L400.3 400.9c0 0 0.1 0 0.1 0l265 537.3c23.5-11.6 46.2-24.8 67.9-39.3L400.5 400.8c0 0 0.1 0 0.1-0.1l395 450.4c19.7-17.3 38.2-35.8 55.5-55.5l-450.4-395c0 0 0-0.1 0.1-0.1L899 733.4c14.5-21.7 27.6-44.4 39.3-68l-537.4-265c0 0 0-0.1 0-0.1l567.4 192.6c8.3-24.6 15.1-49.9 20.2-75.8L401 400.2c0 0 0-0.1 0-0.1L998.7 439.2z'/%3E%3C/g%3E%3C/svg%3E");
                background-size: cover;
            }


            .card-header{
                overflow: hidden;
                position: relative;
                border: 0;
                transition: all 0.5s ;
            }
            .card:hover .card-header{
                padding-right: 50px;
            }
            /*
            .card-header::after{
                position: absolute;
                content: ' ';
                transition: all 0.5s ;
                height: 20px;
                width: 20px;
                right: 20px;
                border-radius: 50%;
                border: 2px solid white;
                top: 12px;
                opacity: 0;
            }
            .card-header::before{
                position: absolute;
                background-color: #f3f3f0;
                content: ' ';
                transition: all 0.5s ;
                border-radius: 50%;
                height: 10px;
                width: 10px;
                right: 25px;
                top: 17px;
                opacity: 0;
            }

            .card:hover .card-header::after{
                animation: circleAnimation 2s infinite;
            }

            .card:hover .card-header::before{
                opacity: 0.0;
            }



            @keyframes circleAnimation {
                00% {
                    height: 20px;
                    width: 20px;
                    right: 20px;
                    opacity: 0.5;
                }
                100% {
                    height: 30px;
                    width: 30px;
                    right: 15px;
                    top: 8px;
                    opacity: 0;
                }
            }
*/


            .card-header::after{
                position: absolute;
                height: 300px;
                background-color: inherit;
                content: " ";
                top: -50px;
                border-left: 0 solid #7f7f7f;
                width: 0;
                right: 0;
                transition: all 0.5s ;
                transform: rotate(0);
            }

            .card:hover .card-header::after{
                right: 0;
                border-left: 10px solid #fff;
                width: 60px;
                transition: all 0.5s ;
                transform: rotate(20deg);
            }

        </style>
    @endpush
    <div class="bg2">
        <section class="pt-5 " id="ad-introduce">
            <div class="container">
                <div class="card ">
                    <div class="card-body p-0">
                        <div class="row p-0 m-0">
                            <div class="col-md-7 p-3 ">
                                <div class="card-title h4">
                                    تبلیغات در زود نیوز
                                </div>
                                <p class="text-justify">
                                    امروزه فضای مجازی بستر مناسبی برای تبلیغات دیجیتال است. در حالی که استفاده از اینترنت و شبکه‌های اجتماعی روز به روز در حال افزایش است شما می توانید از این فرصت استفاده کنید و تبلیغات خود را انتشار دهید.
                                </p>
                                <p class="text-justify">
                                    اگر در فضای مجازی مشتریان رضایت مندی داشته باشید آنها می‌توانند تاثیر بسیار خوبی روی رونق کسب و کار شما داشته باشند.
                                </p>
                                <p class="text-justify">
برای تبلیغ در زودنیوز یکی از روش های زیر را انتخاب نمایید.
                                </p>
                            </div>
                            <div class="col-md-5  align-self-end p-0 h-100" id="img-holder">
                                <img src="{{ asset('images/mojri1.png') }}" alt="" class="w-100  ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="plans" class="py-5 fadeIn wow ">
            <div class="container">
                <div class="card">
                    <div class="card-header bg-danger text-white">
                        پلن ها و قیمت ها
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        لطفا محصول مورد نظر خود را از لیست زیر انتخاب کنید.
                                    </div>
                                </div>
                                <div class="row flexrow">
                                    @foreach($plans as $plan)
                                        <div class="col-md-3 p-4 flexcolumn">
                                            <div class="row h-100">
                                                <div class="col-md-12 plan-card p-0 pb-3" data-planid="{{ $plan->id }}">
                                                    <div class="plan-title h5 text-center bg-info p-3 text-white rounded-top">{{ $plan->title }}</div>
                                                    <div class="plan-body small p-1  text-center">{!!  nl2br($plan->short_description) !!}</div>
                                                    <div class="text-right p-3"><a class='show-link'
                                                                                   href="{{ route('user.plans.show',$plan) }}">مشاهده</a>
                                                    </div>
                                                    <div class="plan-price text-center rounded-bottom p-1 h6 text-danger"> {{ number_format($plan->price) }}
                                                        تومان
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection