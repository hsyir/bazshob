@extends("front.layout.app")
@section('content')
    <div class="container">
        @table
        <thead>
        <tr>
            <th>پلن</th>
            <th>مبلغ فاکتور</th>
            <th>قابل پرداخت</th>
            <th>وضعیت پرداخت</th>
            <th>تاریخ ثبت فاکتور</th>
            <th>تاریخ پرداخت</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
            <tr>
                <td>{{ $invoice->plan->title }}</td>
                <td>{{ $invoice->real_amount }}</td>
                <td>{{ $invoice->payable_amount }}</td>
                <td>{!!  $invoice->paid ? "<div class='badge badge-success'>پرداخت شده</div> " : "---"  !!}</td>
                <td>{{ $invoice->created_at_jalali }}</td>
                <td>{{ $invoice->paid ? $invoice->payment_at_jalali : "---" }}</td>
                <td><a href="{{ route('user.invoices.show',$invoice) }}">مشاهده</a></td>
            </tr>
        @endforeach
        </tbody>
        @endtable
    </div>
@endsection