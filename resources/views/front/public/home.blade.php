@extends("front.layout.app")

@section("content")
    @php
        $plans=\App\Models\Plan::all();
    @endphp
    <section id="homeHeader" class="bg-dark text-white backgroundImage-pan pt-5" >
        <div class="container">

            <div class="row ">
                <div class="col-md-7 p-5 wow fadeIn ">
                    <h1 class="h3 mt-3">به <span class="heartBeat">زودنیوز</span> خوش آمدید</h1>

                    <h6 class="h6 wow shake" data-wow-iteration="1" data-wow-delay="1s">
                        رسانه طنز فارسی با بیش از <span class="glow-red">یک میلیون</span> مخاطب فعال
                    </h6>
                    <div>
                        <a href="{{ route("user.plans.index") }}" class="btn btn-outline-light btn-sm ">
                            <i aria-hidden="true" class="fa fa-bar-chart"></i>
                            تبلیغات در زود نیوز
                        </a>
                    </div>
                </div>
                <div class="col-md-4 " id="newsManImgContainer">
                    <img src="{{ asset('images/news.png') }}" alt="" class="w-100 wow fadeIn" id="newsmanImg">
                </div>
            </div>
        </div>
    </section>

    <section id="followers" class="bg1 py-5  ">
            <div class="container">
                <div class="card">
                    <div class="card-title text-center h3">
                        افتخار ما، همراهی شما
                    </div>
                    <div class="text-center"><small>زودنیوز رسانه طنز مستقل فارسی از سال 1397 آغاز به کار نمود، در این مدت موفق شدیم با نگاهی طنز به مسائل سیاسی و اجتماعی ایران عزیز، بیش از 150 ویدئو تولید کنیم.</small></div>
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col">
                                <div class="h6"> همراهان ما در اینستاگرام</div>
                                <div><span class="h6 text-danger">735,000</span> نفر</div>
                            </div>
                            <div class="col">
                                <div class="h6"> همراهان ما در تلگرام</div>
                                <div><span class="h6 text-danger">150,000</span> نفر</div>
                            </div>
                            <div class="col">
                                <div class="h6"> ویدئو های ما در اینستاگرام</div>
                                <div><span class="h6 text-danger">150</span> پست</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section id='last-post' class=" bg1 py-5  ">
        <div class="container">
            <div class="card p-0">
                <div class="card-title text-white h4 text-center p-4 ">
                    آخرین ویدئو زودنیوز در اینستاگرام
                </div>

                <div class="bg-body p-0">
                    <div class="row  justify-content-around ">
                        <div class="col-md-4 p-5 p-md-0 pt-1 ">
                            <iframe src="https://www.instagram.com/p/B0jIJBznIjq/embed" width="100%" height="520" frameborder="0" scrolling="no" allowtransparency="true"></iframe>
                        </div>
                        <div class="col-8 col-sm-6 col-lg-4 px-5 align-self-end  ">
                            <img src="{{ asset('images/khaje1.png') }}" class="w-100">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="team" class="bg1 py-5  ">
        <div class="container">
            <div class="card">
                <div class="card-title text-center h3 p-3">
                    تیم زود نیوز
                </div>
                <div class="card-body">
                    <div class="row justify-content-center ">

                        <div class="col-md-3 memberCard">
                            <div class="">
                                <div class=" overflow-hidden">
                                    <img src="{{ asset('images/team/shafiee.jpg') }}" alt="عکس مجتبی شفیعی" class="  w-100 ">
                                </div>
                            </div>
                            <div class="mt-2" >مجتبی شفیعی</div>
                            <div>
                                <small>نویسنده، بازیگر، صدا پیشه</small>
                            </div>

                            <div class="social-icons">
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-instagram  "></i></a>
                                <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter  "></i></a>
                                <a href="https://www.facebook.com/">
                                    <i id="social-fb" class="fa fa-facebook  "></i></a>
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-telegram  "></i></a>
                            </div>
                        </div>

                        <div class="col-md-3 memberCard">
                            <div class="">
                                <div class=" overflow-hidden">
                                    <img src="{{ asset('images/team/mahjoori.jpg') }}" alt="عکس مجتبی شفیعی" class="  w-100 ">
                                </div>
                            </div>
                            <div class="mt-2" >جواد مهجوری</div>
                            <div>
                                <small>نویسنده، تصویربردار، تدوین، بازیگر</small>
                            </div>

                            <div class="social-icons">
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-instagram  "></i></a>
                                <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter  "></i></a>
                                <a href="https://www.facebook.com/">
                                    <i id="social-fb" class="fa fa-facebook  "></i></a>
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-telegram  "></i></a>
                            </div>
                        </div>

                        <div class="col-md-3 memberCard">
                            <div class="">
                                <div class=" overflow-hidden">
                                    <img src="{{ asset('images/team/bagherpoor.jpg') }}" alt="عکس مجتبی شفیعی" class="  w-100 ">
                                </div>
                            </div>
                            <div class="mt-2" >منیره باقرپور</div>
                            <div>
                                <small class="text-small">نویسنده، بازیگر</small>
                            </div>

                            <div class="social-icons">
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-instagram  "></i></a>
                                <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter  "></i></a>
                                <a href="https://www.facebook.com/">
                                    <i id="social-fb" class="fa fa-facebook  "></i></a>
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-telegram  "></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="text-white text-center" id="advertisement">
        <div class="container">
            <div class="row pt-5">
                <div class="col-md-12 h5">
دنبال یه راه مطمین برای تبلیغ کسب و کار خودتون میگردین ؟
                </div>
                <div class="col-md-12 p-3">
                    <a href="{{ route("user.plans.index") }}" class="btn btn-outline-warning">
                        <i aria-hidden="true" class="fa fa-bar-chart"></i>
                        تبلیغات در زود نیوز
                    </a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <img src="{{ asset('images/khaje2.png') }}" class="w-100">
                </div>
            </div>
        </div>
    </section>

    <section id="socials" class="bg1 py-5  ">
        <div class="container">
            <div class="card">
                <div class="card-title text-center h3 p-3">
مارا در شبکه های اجتماعی دنبال کنید
                </div>
                <div class="card-body">
                    <div class="row justify-content-center ">

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class=" bg-dark text-white  ">
        <div class="container ">
            <div class="row p-3">
                <div class="col-md-12 h4 text-center">
                    <span class="movasagh p-1"> موثق ترین</span> اخبار ایران و جهان را از ما بشنوید
                </div>
            </div>
        </div>
    </section>

@endsection