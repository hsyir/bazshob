<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Hsy\TenancyThemeManager\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function show(Page $page){
        return view('front.page',compact('page'));
    }
}
