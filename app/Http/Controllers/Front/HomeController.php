<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Mail\Websites\WebsiteReadyMail;
use Doctrine\Common\Cache\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $page = \ThemeManager::getHomePage();
        if (!$page)
            abort(404, "به نظر میرسد صفحه نخست برای این سایت تعریف نشده است");
        return view("front.page", compact('page'));
    }
}

