<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResourceCollection;
use Spatie\Tags\Tag;

class TagController extends Controller
{
    public function show(Tag $tag)
    {
        return view('front.tags.show', compact('tag'));
    }

    public function getPosts(Tag $tag)
    {
        $postModel = config('contentManager.postModel');
        $posts = new PostResourceCollection($postModel::withAllTags($tag->name)->paginate(6));
        return $posts;
    }
}