<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResourceCollection;
use App\Models\Post;
use Illuminate\Http\Request;
use \CntManager;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    public function show(Post $post)
    {
        return view('front.posts.show', compact('post'));
    }

    public function ajaxGetPosts(Request $request)
    {

        $posts = CntManager::get("posts")
            ->categoryId($request->has('category_id') ? $request->category_id : null)
            ->tagId($request->has('tag_id') ? $request->tag_id : null)
            ->with("media", "user")
            ->cacheKey($this->cacheKey($request))
            ->paginate(6);

        /*$posts1 = Post
        ::when($request->has("tag_id"), function ($q) use ($request) {
            $tag = Tag::find($request->tag_id);
            return $q->withAllTags($tag->name);
        })
        ->when($request->has("category_id"), function ($q) use ($request) {
            return $q->whereCategoryId($request->category_id);
        })
        ->when($request->has('token'), function ($q) use ($request) {
            return $q;
        })
        ->published()
        ->visible()
        ->with('user')
        ->paginate(2);
        */

        return new PostResourceCollection($posts);
    }

    /**
     * @param $request Request
     * @return string
     */
    private function cacheKey($request)
    {
        $cacheKey = "posts-"
        . $request->has("category_id") ? "cat{$request->category_id}" : ""
        . $request->has("tag_id") ? "tag{$request->tag_id}" : ""
        . $request->has("page") ? "page{$request->page}" : "";
        return $cacheKey;
    }
}
