<?php
namespace Hsy\Categories;

class Facade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return "Categories";
    }
}
