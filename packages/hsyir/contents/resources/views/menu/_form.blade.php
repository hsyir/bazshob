<form action="{{ route('admin.menu.store')  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{--    {{ Html::hidden()->name('parent_id')->value($menu->id)->when( $action=='edit') }}--}}
    {{--{{ Html::method()->value("PUT")->when($action=='edit') }}--}}

    <div class="row">
        <div class="col-md-12">

            <label for="parent_id"> منو پدر</label>
            <select name="parent_id" class="form-control " id="parent_id">
                @foreach($menuTree as $m)
                    <option {{ $m['id']==old("parent_id",$menu->id) ? "selected" : ""}} value="{{ $m['id'] }}">{{ $m['title']}}</option>
                @endforeach
            </select>
            {{ Html::text("title")->label("عنوان منو")->value(old("title")) }}
            {{ Html::text("url")->label("لینک")->value(old("url")) }}
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="submit" value="ثبت" class="btn btn-success">
            <a href="{{ route("admin.menu.index") }}" class="btn btn-danger">بازگشت</a>
        </div>
    </div>
</form>