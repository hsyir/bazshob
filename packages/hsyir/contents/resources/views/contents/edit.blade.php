@extends ('back.layout.app')

@section ('content')
    @component ('back/components/headerWithToolbar',['title'=>'ویرایش'])
        <a class="text-secondary" href="{{route('admin.contents.index',$cntManager->postType->getType())}}"> بازگشت @icon(arrow-left)</a>
    @endcomponent
    @success @endsuccess
    @errors @enderrors
    <div class="card mb-3">
        <div class="card-header">ویرایش  <span class="text-primary">{{ $post->title }}</span></div>
        <div class="card-body">
            @include('contents::contents/_form',['action'=>'edit'])
        </div>
    </div>
    
@endsection
