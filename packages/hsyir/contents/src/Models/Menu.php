<?php

namespace Hsy\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Translatable\HasTranslations;

class Menu extends Model
{
    use HasMediaTrait,  HasTranslations, NodeTrait;

    protected $fillable = ['title', 'name', 'url', 'icon','parent_id'];
    public $translatable = ['title'];

}
