<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 10/3/2019
 * Time: 8:53 AM
 */

namespace Hsy\Content;


use Hsy\Content\Traits\CacheManager;
use Hsy\Content\Traits\HasErrors;
use Hsy\Content\Classes\PostType;
use Hsy\Content\Traits\TagsTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Hsy\Content\Models\Post;
use Illuminate\Support\Facades\Validator;

class ContentManager
{
    use HasErrors, CacheManager, TagsTrait;

    /**
     * @var $post Post
     */
    public $post;

    /**
     * @var $postType PostType
     */
    public $postType;

    public function __construct($type = null)
    {
        if (!$type)
            $type = request()->route() ? request()->route()->postType : null;

        $this->setPostType($type);
    }

    public function setPostType($type)
    {
        $model = config('contentManager.postModel');
        if (!$type) {
            $this->postType = null;
            $this->post = new $model;
            return;
        }
        $this->postType = new PostType($type);
        $this->post = new $model;
        $this->post->type = $type;
        $this->post->setTypeGlobalScope($type);
        return $this->postType;
    }


    /**
     * @return array
     */
    public function getMenuItems()
    {
        $menuItems = [];
        foreach (config('contentManager.types') as $type => $options) {
            $menuItems[$type] = $options['title'];
        }
        return $menuItems;
    }

    public function registerMediaCollections($post)
    {
        foreach ($this->postType->getMedias() as $media) {
            ($media->mediaCollection)($post);
        }
    }

    /**
     * @param $request Request
     * @return mixed
     */
    private function validateRequest($request)
    {
        $rules = $request->isMethod('post')
            ? $this->postType->getStoreValidationRules()
            : $this->postType->getUpdateValidationRules();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $this->mergeErrors($validator->errors());
            return false;
        }
        return true;
    }

    /**
     * Store a newly created or Update the specified resource in storage.
     * @param $post Post
     * @param $request Request
     * @return mixed
     * @throws \Exception
     */
    public function storePost($post, $request)
    {
        //reset slug
        $post->slug = null;

        $data = $request->all();
        if (!$this->validateRequest($request))
            return false;

        $data['published'] = $request->has('published') ? true : false;
        $data['visible'] = $request->has('visible') ? true : false;

        $data['user_id'] = Auth::user() ? Auth::user()->id : null;

//        DB::connection("tenant")->beginTransaction();

        try {
            $this->postType->registerMediaCollections($post);

            $post->fill($data);
            $post->save();

            foreach ($this->postType->getMedias() as $media) {
                if ($request->has($media->name))
                    $post->addMediaFromRequest($media->name)->toMediaCollection($media->name);
            }

//            DB::connection("tenant")->commit();
            $this->cacheFlush();

        } catch (\Exception $e) {
//            DB::connection("tenant")->rollBack();
            $this->addError('exception', $e->getMessage());
            return false;
        }


        if ($this->postType->isTaggable())
            $post->syncTags($request->tags);

        return $post;
    }

    public function getSoloInfo($slug, $default = null)
    {
        $cacheKey = "posts_solo_{$slug}";
        return Cache::remember($cacheKey, self::CACHE_TIME, function () use ($slug, $default) {
            $info = $this->post::whereType('solo_info')->whereSlug($slug)->withoutGlobalScope('postType')->first();
            if ($info)
                return $info->body;

            return ($default ? $default : "posts_{$slug}");
        });
    }

    /**
     * @param $type
     * @param bool $descOrder
     * @param null $limit
     * @param null $offset
     * @return mixed
     */
    public function getPosts($type, $category_id = null, $descOrder = false, $limit = null, $offset = null)
    {
        $cacheKey = $this->cacheKey("posts", $type, $category_id, $descOrder, $limit, $offset);

        if ($this->cacheHas($cacheKey))
            return $this->cacheGet($cacheKey);

        $this->setPostType($type);

        $posts = $this->post->whereType($type)
            ->when($descOrder, function ($q) {
                return $q->orderBy('created_at', 'DESC');
            })
            ->when($this->postType->getMedias() != [], function ($q) {
                return $q->with('media');
            })
            ->when($offset, function ($q) use ($offset) {
                return $q->offset($offset);
            })
            ->when($limit, function ($q) use ($limit) {
                return $q->limit($limit);
            })
            ->when($category_id, function ($q) use ($category_id) {
                return $q->inCategoryTree($category_id);
            })
            ->published()
            ->get();
        $this->cacheRemember($cacheKey, $posts);
        return $posts;
    }

    /**
     * @param null $type
     * @return PostQuery
     */
    public function get($type = null, $category_id = null)
    {
        $postQuery = new PostQuery($type, $category_id);
        return $postQuery;
    }


    /**
     * @param $tags
     * @param null $count
     * @return
     */
    public function getRelatedPosts($tags, $count = null)
    {
        return $this->post->published()->visible()->withAnyTags($tags)->when($count, function ($q) use ($count) {
            return $q->limit($count);
        })->get();
    }

}