<?php

namespace Hsy\Store\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Attribute extends Model
{
    use HasTranslations;

    protected $fillable = ['title', 'unit', 'type', 'comment'];
    public $translatable = ['title', 'unit'];
}
