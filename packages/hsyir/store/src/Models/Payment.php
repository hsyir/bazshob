<?php

namespace Hsy\Store\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    const STATUS_FAIL = -1;
    const STATUS_PENDING = 0;
    const STATUS_PAID = 1;

    public function isPaid()
    {
        return $this->status == self::STATUS_PAID;
    }

    public function confirmPayment($refId)
    {
        $this->status = self::STATUS_PAID;
        $this->save();
    }


    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
}
