<?php

namespace Hsy\Store\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    public function items()
    {
        return $this->hasMany(InvoiceItem::class);
    }


    public function getIsPaidAttribute()
    {
        return $this->paid_at != false;
    }

    public function confirmPayment()
    {
        $this->paid_at = Carbon::now();
        $this->save();
    }
}
