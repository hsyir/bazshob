<?php

namespace Hsy\Store\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductVarietyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->product->title,
            "price" => $this->price,
            "discount_percent" => $this->discount_percent
        ];
    }
}
