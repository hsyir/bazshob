@extends ('back.layout.app')

@section ('content')
    @component ('back/components/headerWithToolbar',['title'=>"ویرایش محصول"])
        <a class="text-secondary" href="{{route('admin.products.index')}}"> بازگشت
            @icon(arrow-left)</a>
    @endcomponent
    @success @endsuccess
    @errors @enderrors
    @include('store::products._form',['action'=>'edit'])
@endsection
