<?php

namespace Hsy\ThemeManager\Facades;

use Illuminate\Support\Facades\Facade;
use Hsy\ThemeManager\ThemeOptions as TO;

class ThemeOptions extends Facade
{
    protected static function getFacadeAccessor()
    {
        return TO::class;
    }
}
