<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 26/Nov/2019
 * Time: 08:03 AM
 */

namespace Hsy\ThemeManager\Repositories;


use Hsy\ThemeManager\Traits\HasErrors;

class PageRepository
{
    use HasErrors;

    public function storePage($page, $request)
    {
        $data = $request->all();
        if (!$this->validateRequest($request))
            return false;

        try {
            $page->fill($data);
            $page->save();

        } catch (\Exception $e) {
            $this->addError('exception', $e->getMessage());
            return false;
        }

        return $page;
    }

    private function validateRequest($request)
    {
        $rules = [
            'title' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $this->merge($validator->errors());
            return false;
        }
        return true;
    }

}