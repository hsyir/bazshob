<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 26/Nov/2019
 * Time: 08:03 AM
 */

namespace Hsy\ThemeManager\Repositories;


use Hsy\ThemeManager\Models\Theme;
use Hsy\ThemeManager\Traits\HasErrors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ThemeRepository
{
    use HasErrors;

    /**
     * @param $theme Theme
     * @param $request Request
     * @return bool|Theme
     */
    public function storeTheme($theme, $request)
    {

        $data = $request->all();
        if (!$this->validateRequest($request))
            return false;

        DB::connection("system")->beginTransaction();

        try {

            $theme->fill($data);
            $theme->save();

            DB::connection("system")->commit();

        } catch (\Exception $e) {
            DB::connection("system")->rollBack();
            $this->addError('exception', $e->getMessage());
            return false;
        }

        return $theme;
    }


    /**
     * @param $request Request
     * @return bool
     */
    private function validateRequest($request)
    {
        $rules = [
            'title' => 'required',
            'name' => 'required|unique:system.themes',
        ];

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $this->merge($validator->errors());
            return false;
        }
        return true;
    }
}