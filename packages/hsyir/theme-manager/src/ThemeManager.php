<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 26/Nov/2019
 * Time: 08:03 AM
 */

namespace Hsy\ThemeManager;


use Hsy\ThemeManager\Models\Page;

class ThemeManager
{
    public function getHomePage()
    {
        $homePageId = \ThemeOptions::get('homepage');
        return Page::find($homePageId);
    }


    /**
     * @param $page Page
     * @return mixed
     */
    public function getPageTemplate($page)
    {
        return json_decode($page->template ? $page->template : "[]");
    }
}