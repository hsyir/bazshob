<?php

namespace Hsy\ThemeManager\Middlewares;

use Closure;
use Igaster\LaravelTheme\Facades\Theme;

class SetSiteTheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $theme = \Options::get('site_theme_name');
        Theme::set($theme);
        return $next($request);
    }
}
