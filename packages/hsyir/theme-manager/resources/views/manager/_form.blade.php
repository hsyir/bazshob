<form action="{{ route('manager.themes.store')  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('theme_id')->value($theme->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}
    <div class="row">
        <div class="col-md-12">
            {{ Html::text("name")->label("نام / لاتین")
                ->value(old('name',$theme->name))
            }}
            {{ Html::text("title")->label("عنوان فارسی")
                ->value(old('title',$theme->title))}}

            {{ Html::textarea("description")
                ->value(old('description',$theme->description))
                ->label("توضیحات")
                ->description("")
                ->attributes(['row'=>20]) }}
            {{ Html::submit()->label("ثبت") }}
        </div>
    </div>
</form>